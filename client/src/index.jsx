import React from 'react';
import ReactDOM from 'react-dom';
import OfflinePluginRuntime from 'offline-plugin/runtime';
import { AppContainer } from 'react-hot-loader';
import RootContainer from './containers/RootContainer';

// Import css
import './styles/main.scss';

OfflinePluginRuntime.install();

const render = (AppComponent) => {
  ReactDOM.render(
    <AppContainer>
      <AppComponent />
    </AppContainer>,
    document.querySelector('#root'));
};

render(RootContainer);

// Hot Module Replacement API
if (module.hot) {
  module.hot.accept('./containers/RootContainer', () => {
    const nextRootContainer = require('./containers/RootContainer').default;
    render(nextRootContainer);
  });
}
