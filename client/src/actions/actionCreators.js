import {
  INCREMENT_LIKES,
  ADD_COMMENT,
  REMOVE_COMMENT
} from '../constants/action_types';

export function incrementLike(index) {
  return {
    type: INCREMENT_LIKES,
    index,
  };
}

export function addComment(postId, author, comment) {
  return {
    type: ADD_COMMENT,
    postId,
    author,
    comment,
  };
}

export function removeComment(postId, i) {
  console.log('removeCOmment acts')
  return {
    type: REMOVE_COMMENT,
    i,
    postId,
  };
}
