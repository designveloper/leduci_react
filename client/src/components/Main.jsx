import React from 'react';
import PropTypes from 'prop-types';
import PhotoGrid from './PhotoGrid';
// No need for import data
// import posts from '../data/posts';
// import comments from '../data/comments';

const Main = props => (
  <div>
    <h1>PhotoAlbum</h1>
    <PhotoGrid
      posts={props.posts}
      comments={props.comments}
      incrementLike={props.incrementLike}
    />
  </div>
);

Main.propTypes = {
  posts: PropTypes.array.isRequired,
  comments: PropTypes.object.isRequired,
  incrementLike: PropTypes.func.isRequired,
};

export default Main;
