import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actionCreators from '../actions/actionCreators';

import Photo from './Photo';
import Comments from './Comments';

// import data
// import posts from '../data/posts';
// import comments from '../data/comments';

const SinglePhoto = (props) => {

  const { postId } = props.match.params;
  const { comments, posts } = props;

  const i = posts.findIndex(post => post.code === postId);
  const post = posts[i];
  const postComments = comments[postId] || [];

  return (
    <div className="single-photo">
      Hello
      <Photo
        i={i}
        post={post}
        comments={comments}
        incrementLike={props.incrementLike}
      />
      <Comments
        postId={postId}
        postComments={postComments}
        removeComment={props.removeComment}
        addComment={props.addComment}
      />
    </div>
  );
};

SinglePhoto.propTypes = {
  removeComment: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  incrementLike: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
  comments: PropTypes.object.isRequired,
  posts: PropTypes.array.isRequired,
  history: PropTypes.object.isRequired,
};


const mapStateToProps = (state) => {
  return {
    posts: state.posts,
    comments: state.comments,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(actionCreators, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(SinglePhoto);
