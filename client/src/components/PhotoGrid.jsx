import React from 'react';
import PropTypes from 'prop-types';

import Photo from './Photo';

const PhotoGrid = ({ posts, comments, incrementLike }) => (
  <div className="photo-grid">
    {posts.map((post, i) => (
      <Photo
        post={post}
        key={i}
        i={i}
        comments={comments}
        posts={posts}
        incrementLike={incrementLike}
      />)
    )}
  </div>
);

PhotoGrid.defaultProps = {
  posts: [],
  comments: [],
};

PhotoGrid.propTypes = {
  posts: PropTypes.array.isRequired,
  comments: PropTypes.object.isRequired,
  incrementLike: PropTypes.func.isRequired,
};

export default PhotoGrid;
