import React from 'react';
import PropTypes from 'prop-types';

const Comments = ({ postComments, postId, removeComment, addComment }) => {
  let _author, _comment, _form;

  const renderComment = (comment, i) => {
    return (
      <div className="comment" key={i}>
        <p>
          <strong>{comment.user}</strong>
          {comment.text}
          <button className="remove-comment" onClick={removeComment.bind(null, postId, i)}>&times;</button>
        </p>
      </div>
    );
  };

  const handleFormSubmit = (e) => {
    e.preventDefault();
    const author = _author.value;
    const comment = _comment.value;

    addComment(postId, author, comment);

    _form.reset();
  };

  return (
    <div className="comments">
      {postComments.map(renderComment)}
      <form ref={form => _form=form} className="comment-form" onSubmit={handleFormSubmit}>
        <input type="text" ref={input => _author=input} placeholder="author"/>
        <input type="text" ref={input => _comment=input} placeholder="comment"/>
        <input type="submit" hidden />
      </form>
    </div>
  );
};

Comments.propTypes = {
  postComments: PropTypes.array.isRequired,
  postId: PropTypes.string.isRequired,
  removeComment: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
};

export default Comments;