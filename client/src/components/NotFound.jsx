import React from 'react';

const NotFound = () => (
  <h1>Sorry! No page found!</h1>
);

export default NotFound;