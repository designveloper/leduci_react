import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import CSSTransitionGroup from 'react-addons-css-transition-group';

const Photo = (props) => {
  const { post, i, comments, incrementLike } = props;
  return (
    <div className="grid-figure">
      <div className="grid-photo-wrap">
        <Link to={`/view/${post.code}`}>
          <img src={post.display_src} alt={post.caption} className="grid-photo" />
        </Link>

        <CSSTransitionGroup transitionName="like" transitionEnterTimeout={500} transitionLeaveTimeout={500}>
          <span key={post.likes} className="likes-heart">{post.likes}</span>
        </CSSTransitionGroup>

        <figcaption>
          <p>{post.caption}</p>
          <div className="control-buttons">
            <button onClick={incrementLike.bind(null, i)} className="likes">&hearts; {post.likes}</button>
            <Link className="button" to={`/view/${post.code}`}>
              <span className="comment-count">
                <span className="speech-bubble" />
                {comments[post.code] ? comments[post.code].length : 0}
              </span>
            </Link>
          </div>
        </figcaption>
      </div>
    </div>
  );
};

Photo.propTypes = {
  i: PropTypes.number.isRequired,
  post: PropTypes.object.isRequired,
  comments: PropTypes.object.isRequired,
  incrementLike: PropTypes.func.isRequired,
};

export default Photo;