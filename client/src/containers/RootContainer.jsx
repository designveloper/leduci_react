import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';

// import Root from './Root';
import Root from './Root';
import SinglePhoto from '../components/SinglePhoto';
import NotFound from '../components/NotFound';

import store, { history } from '../store';

const RootContainer = () => (
  <Provider store={store}>
    <Router history={history}>
      <Switch>
        <Route exact path="/" component={Root} />
        <Route path="/view/:postId" component={SinglePhoto} />
        <Route component={NotFound} />
      </Switch>
    </Router>
  </Provider>
);

export default RootContainer;
