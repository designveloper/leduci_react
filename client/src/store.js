import { createStore } from 'redux';
import createBrowserHistory from 'history/createBrowserHistory';
import rootReducer from './reducers/index';

// import data
import posts from './data/posts';
import comments from './data/comments';

const store = createStore(rootReducer, { posts, comments });

export const history = createBrowserHistory();

if (module.hot) {
  module.hot.accept('./reducers/', () => {
    const nextRootReducer = require('./reducers/index').default;
    store.replaceReducer(nextRootReducer);
  });
}

export default store;
