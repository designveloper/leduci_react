const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require('autoprefixer');
const OfflinePlugin = require('offline-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
  context: path.resolve('client/src/'),
  devtool: 'cheap-module-source-map',
  entry: {
    main: [
      'babel-polyfill', // For using Promise,
      'react-hot-loader/patch', // RHL patch // For hot reloading page when any changes in saving files
      './index.jsx',
    ],
  },
  output: {
    filename: 'js/[name].js',
  },
  module: {
    strictExportPresence: true, // makes missing exports an error instead of warning
    rules: [
      {
        oneOf: [ // excute only one match
          {
            test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
            loader: 'url-loader',
            options: {
              limit: 10000,
              name: 'static/media/[name].[hash:8].[ext]',
            },
          },
          {
            test: /\.(jsx|js)$/,
            enforce: 'pre', // inline loader post > inline > normal > pre, default: normal
            exclude: /(node_modules)/, // no match file in there
            use: [
              'react-hot-loader/webpack',
              {
                loader: 'babel-loader', // babel-loader?cacheDirectory=true
                options: {
                  presets: [
                    ['env', { modules: false }],
                    'react',
                  ],
                  plugins: [require('babel-plugin-transform-object-rest-spread'), require('babel-plugin-transform-runtime')],

                  cacheDirectory: true, // cache result of loader
                },
              },
            ],
          },
          {
            test: /\.(scss|css)$/,
            use: [
              { loader: 'style-loader' },
              { loader: 'css-loader', options: { importLoaders: 1 } },
              {
                loader: 'postcss-loader',
                options: {
                  ident: 'postcss',
                  plugins: () => [
                    require('postcss-flexbugs-fixes'),
                    autoprefixer({
                      browsers: [
                        '>1%',
                        'last 4 versions',
                        'Firefox ESR',
                        'not ie < 9', // React doesn't support IE8 anyway
                      ],
                      flexbox: 'no-2009',
                    }),
                  ],
                },
              },
              { loader: 'sass-loader' },
            ],
          },
          {
            exclude: [/\.js$/, /\.html$/, /\.json$/],
            loader: 'file-loader',
            options: {
              name: 'static/media/[name].[hash:8].[ext]',
            },
          },
          {
            test: /\.styl$/,
            include: path.join(__dirname, 'client'),
            loader: 'style-loader!css-loader!stylus-loader',
          },
        ],
      },
    ],
  },
  resolve: {
    extensions: ['.jsx', '.js', '.scss', '.json', '.css'],
  },
  devServer: {
    host: '0.0.0.0',
    port: '3000',
    contentBase: path.join(__dirname, 'public'),
    compress: true, // enable gzip compression
    historyApiFallback: true, // true for index.html upon 404, object for multiple paths
  },
  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      template: '../public/index.html',
    }),
    new OfflinePlugin({ caches: { main: [] } }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': '"development"',
    }),
    new webpack.HotModuleReplacementPlugin(),
    new BundleAnalyzerPlugin({
      // Can be `server`, `static` or `disabled`. 
      // In `server` mode analyzer will start HTTP server to show bundle report. 
      // In `static` mode single HTML file with bundle report will be generated. 
      // In `disabled` mode you can use this plugin to just generate Webpack Stats JSON file by setting `generateStatsFile` to `true`. 
      analyzerMode: 'server',
      // Host that will be used in `server` mode to start HTTP server. 
      analyzerHost: '127.0.0.1',
      // Port that will be used in `server` mode to start HTTP server. 
      analyzerPort: 8888,
      // Path to bundle report file that will be generated in `static` mode. 
      // Relative to bundles output directory. 
      reportFilename: 'report.html',
      // Module sizes to show in report by default. 
      // Should be one of `stat`, `parsed` or `gzip`. 
      // See "Definitions" section for more information. 
      defaultSizes: 'parsed',
      // Automatically open report in default browser 
      openAnalyzer: true,
      // If `true`, Webpack Stats JSON file will be generated in bundles output directory 
      generateStatsFile: false,
      // Name of Webpack Stats JSON file that will be generated if `generateStatsFile` is `true`. 
      // Relative to bundles output directory. 
      statsFilename: 'stats.json',
      // Options for `stats.toJson()` method. 
      // For example you can exclude sources of your modules from stats file with `source: false` option. 
      // See more options here: https://github.com/webpack/webpack/blob/webpack-1/lib/Stats.js#L21 
      statsOptions: null,
      // Log level. Can be 'info', 'warn', 'error' or 'silent'. 
      logLevel: 'info',
    }),
  ],
  node: {
    dgram: 'empty',
    fs: 'empty',
    net: 'empty',
    tls: 'empty',
    child_process: 'empty',
  },
};
